#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Patron'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Manage Patron" } } +
                query.body do
                        query.form("ACTION" => "prog/removePatronUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Remove Library Patron" } +
                                query.hr +
                                "<p>Enter memberID of Patron to remove:   " + query.text_field("memberID") +
                                query.submit("VALUE" => "Search") +
                                "</p>"
                        end
                end
        end
end


