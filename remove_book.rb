#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Book'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Search" } } +
                query.body do
                        query.form("ACTION" => "prog/removeBookUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Remove Book from Inventory" } +
                                query.hr +
                                "<p>Enter ISBN to remove:   " + query.text_field("isbn") +
                                query.submit("VALUE" => "Search") +
                                "</p>"
                        end
                end
        end
end

