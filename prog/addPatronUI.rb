#!/usr/bin/ruby
require 'cgi'
require_relative '../lib/Patron'

query = CGI.new("html5")

addPatron = Patron.new
patronName = addPatron.addMember(query['firstName'], query['lastName'], query['email'])

query.out do
query.head { query.title { "SmartLibrary: Manage Patron" } } +
query.form do
        query.h1{ "Add new Library Patron" } +
        query.hr +
        query.h3{"You have successfully added #{patronName} as a member!"} +
        query.br +
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end

