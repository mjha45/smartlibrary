#!/usr/bin/ruby  

#Require the CGI class and our custom Book class
require 'cgi' 
require_relative '../lib/Book.rb'

#Create CGI object
query = CGI.new("html5")

#Print statement for HTML
print "Content-type: text/html\r\n\r\n"
print "<h1>Search Results</h1><hr>"

#Create book object 
#Then call the search Inventory function in Book.rb
#And store it into a new local array
mybook = Book.new
searchResults = []
searchResults << mybook.searchInventory(query['isbn'])

#Loop through returned array of books and print to screen
$x = 0
$numResults = searchResults[0].size
print "<h3>Your query returned #{$numResults} results:</h3><br></br>"
searchResults.each do |y|
   while $x < $numResults
	puts "RESULT #{$x + 1}:"
	print "<br></br>"
	puts "ISBN: "
	puts y[$x][0]
	print "<br></br>"
	puts "TITLE: "
	puts y[$x][1]
	print "<br></br>"
	puts "AUTHOR: "
	puts y[$x][2]
	print "<br></br>"
	puts "LIBRARY LOCATION: "
	puts y[$x][3]
	print "<br></br>"
	puts "CURRENT STATUS: "
	puts y[$x][4]
	print "<br></br><br></br>"
	$x = $x + 1
   end 
end

#use CGI object to format our Form with a page name, and a link back to the main menu
query.out do
query.html do                 
query.head { query.title { "SmartLibrary: Search" } } +
query.body do                         
query.form do
	query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end

