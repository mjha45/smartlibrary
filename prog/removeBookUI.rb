#!/usr/bin/ruby  
require 'cgi'
require_relative '../lib/Book'

query = CGI.new("html5")

book = Book.new
successresult = book.removeBook(query['isbn'])
confirmation = ""

if (successresult == true)
	confirmation = "Item successfully removed!"
else
	confirmation = "Error removing book: Please check the ISBN try again"
end

query.out do
query.html do
query.head { query.title { "SmartLibrary: Remove Item" } } +
query.body do
query.form do
        query.h1{ "Remove Item from Inventory" } +
        query.hr +
        query.h3{"#{confirmation}"} +
        query.br +
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end
