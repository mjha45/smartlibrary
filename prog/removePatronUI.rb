#!/usr/bin/ruby
require 'cgi'
require_relative '../lib/Patron'

query = CGI.new("html5")

patron = Patron.new
successresult = patron.removeMember(query['memberID'])
confirmation = ""

if (successresult == true)
        confirmation = "Member successfully removed!"
else
        confirmation = "Error removing patron: Please check the memberID and try again"
end

query.out do
query.html do
query.head { query.title { "SmartLibrary: Remove Patron" } } +
query.body do
query.form do
        query.h1{ "Remove Patron from Inventory" } +
        query.hr +
        query.h3{"#{confirmation}"} +
        query.br +
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end

