#!/usr/bin/ruby  

#Require the CGI class and our custom Patron class
require 'cgi'
require_relative '../lib/Patron'

#Create CGI object
query = CGI.new("html5")

#Print statement for HTML
print "Content-type: text/html\r\n\r\n"
print "<h1>Search Results</h1><hr>"

#Create book object 
#Then call the search Inventory function in Book.rb
#And store it into a new local array
myPatron = Patron.new
searchResults = []
searchResults << myPatron.searchPatron(query['lastName'])

#Loop through returned array of Patrons and print to screen
$x = 0
$numResults = searchResults[0].size
print "<h3>Your query returned #{$numResults} results:</h3><br></br>"
searchResults.each do |y|
   while $x < $numResults
        puts "RESULT #{$x + 1}:"
        print "<br></br>"
        puts "FIRST NAME: "
        puts y[$x][1]
        print "<br></br>"
        puts "LAST NAME: "
        puts y[$x][2]
        print "<br></br>"
        puts "MEMBER ID: "
        puts y[$x][0]
        print "<br></br><br></br>"
        $x = $x + 1
   end
end 

#use CGI object to format our Form with a page name, and a link back to the main menu
query.out do
query.html do
query.head { query.title { "SmartLibrary: Search Patron" } } +
query.body do
query.form do
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end

