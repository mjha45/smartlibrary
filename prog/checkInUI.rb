#!/usr/bin/ruby  
require 'cgi'
require_relative '../lib/Transaction'
require_relative '../lib/Patron'
require_relative '../lib/Book'

query = CGI.new("html5")

transaction = Transaction.new
returnResults = transaction.checkIn(query['isbn'])
confirmation = ""

if (returnResults == "Invalid ISBN")
        confirmation = "Error processing transaction: Please check the ISBN and try again"
elsif (returnResults == "Already Available")
	confirmation = "Error processing transaction: Book is already in Available status"
else
        confirmation = "Check In Transaction Successful!"
end

query.out do
query.html do
query.head { query.title { "SmartLibrary: Check In" } } +
query.body do
query.form do
        query.h1{ "Check In a Book" } +
        query.hr +
        query.h3{"#{confirmation}"} +
        query.br +
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end



