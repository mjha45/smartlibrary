#!/usr/bin/ruby  
require 'cgi'
require_relative '../lib/Transaction'
require_relative '../lib/Patron'
require_relative '../lib/Book'

query = CGI.new("html5")

transaction = Transaction.new
returnResults = transaction.checkOut(query['isbn'], query['memberID'])
confirmation = ""

if (returnResults == "Invalid ISBN")
        confirmation = "Error processing transaction: Please check the ISBN and try again"
elsif (returnResults == "Invalid memberID")
        confirmation = "Error processing transaction: Please check the memberID and try again"
elsif (returnResults == "Already Out")
	confirmation = "Error procesing transaction: Book is already checked out"
else
	confirmation = "Transaction Successful! The Due Date for this book is: <br></br> #{returnResults}"
end

query.out do
query.html do
query.head { query.title { "SmartLibrary: Check Out" } } +
query.body do
query.form do
        query.h1{ "Check Out a Book" } +
        query.hr +
        query.h3{"#{confirmation}"} +
        query.br +
        query.a("http://www.mayankjha.com/SmartLibrary/admin_menu.html"){ "BACK TO MAIN MENU" }
end
end
end
end
