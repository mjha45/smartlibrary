#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Patron'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Add Patron" } } +
                query.body do
                        query.form("ACTION" => "prog/addPatronUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Add a new Library Patron" } +
                                query.hr +
				"<p>Enter Patron Information:</p><br></br>" +
                                "<p>First Name:   " + query.text_field("firstName") + "</p>" +
                                "<p>Last Name:  " + query.text_field("lastName") + "</p>" +
                                "<p>Email: " + query.text_field("email") + "</p>" +
                                query.submit
                        end
                end
        end
end


