#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Book'
require_relative '../lib/Transaction'
require_relative '../lib/Patron'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Manage Transactions" } } +
                query.body do
                        query.form("ACTION" => "prog/checkInUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Check In a Book" } +
                                query.hr +
                                "<p>Enter ISBN of book to Check In:   " + query.text_field("isbn") + "</p>" +
                                query.submit
                        end
                end
        end
end
