#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Smartlibrary/Book'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Add Book" } } +
                query.body do
                        query.form("ACTION" => "prog/addBookUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Add a Book to the Inventory" } +
                                query.hr +
                                "<p>Enter ISBN:   " + query.text_field("isbn") + "</p>" +
				"<p>Enter Title:  " + query.text_field("title") + "</p>" + 
				"<p>Enter Author: " + query.text_field("author") + "</p>" + 
				"<p>Enter Library Location: " + query.text_field("location") + "</p>" + 
                                query.submit
                        end
                end
        end
end
