#==This class serves the following functionality:
#* <b>Check out a book from the library and calculate a due date</b>
#* <b>Check in a book back to the library inventory</b>
#* <b>Place a book on hold if it is checked out or available</b>

#module Smartlibrary

class Transaction
require 'csv'
require 'date'
require 'mysql2'
	#This is the unique ID of the library transaction (automatically system generated)
	attr_reader   :transactionID
	
	#This is the action taken. Can be one of the following:
	#*check-out
	#*check-in
	#*reserve
	attr_accessor :action
	
	#This is the ISBN of the book on which we are performing a transaction
	attr_accessor :isbn

	#This is the memberID of the patron for whom we are performing the transaction
	attr_accessor :memberID

	#This is the due date of a book that is checked out (calculated by the check out function)
	attr_accessor :dueDate

	#When constructing an object of this class, we set all instance variables to blank, except for +transactionID+, which 
	#is automatically system generated for each function in this class by locating the max transactionID from the first column 
	#of the CSV file containing all library transactions, and adding 1.
	def initialize
		@action = ""
		@isbn = ""
		@memberID = ""
		@dueDate = ""
	end

	#This method allows us to check out a book from the library inventory. The +memberID+ is also needed in order to assign the 
	#book to a patron. If the memberID is not in the CSV file containing all members, the method will exit.
	#
	#The Due Date is then set to 3 weeks from the exact date and time of the transaction
	#*Example:*
	#	someTransaction = Transaction.new
	#	someTransaction.checkOut("1234567891011","100")
	#
	def checkOut(isbn, memberID)
		#Set value of action, isbn, and memberID fields for transaction data
                @action = "check-out"
                @isbn = isbn
                @memberID = memberID

                #Set return value for UI layer
                returnValue = ""

                #Connect to the MySQL server
                con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary")
                con.query_options.merge!(:as => :array)

                #Validate ISBN
                validateISBNQuery = con.query("SELECT * FROM Book_inventory WHERE ISBN = '#{isbn}'")

                #If sql query returns no results, then there is no such book in the DB, so exit the function
                if (validateISBNQuery.count == 0)
                        returnValue = "Invalid ISBN"
                        return returnValue
                end

                #Validate member
                validateMemberQuery = con.query("SELECT * FROM Member_list WHERE memberID = '#{memberID}'")

                #If sql query returns no results, then there is no such book in the DB, so exit the function
                if (validateMemberQuery.count == 0)
                        returnValue = "Invalid memberID"
                        return returnValue
                end

                #Get InventoryID of book to be checked out
                inventoryIDQuery = con.query("SELECT Inventory_ID FROM Book_inventory WHERE ISBN = '#{isbn}' AND Status = 'Available' ORDER BY Inventory_ID LIMIT 1")

		#If query returns no results, all matching books are already checked out, so exit the function
                if (inventoryIDQuery.count == 0)
                        returnValue = "Already Out"
                        return returnValue
                end
		
		#Set the value of the unique book ID for Transaction data
		inventoryID = ""
                inventoryIDQuery.each do |row|
                        inventoryID = row[0].to_int
                end


                #Inner Query - Retrieve uniqueID of the first 'Available' book matching the requested ISBN
                #Outer Query - Update the Status of that retrieved book to Checked Out
                updateQuery = con.query("UPDATE Book_inventory b JOIN
                        (SELECT Inventory_ID FROM Book_inventory WHERE ISBN = '#{isbn}' AND Status = 'Available' ORDER BY Inventory_ID LIMIT 1) s
                        ON b.Inventory_ID = s.Inventory_ID
                        SET Status = 'Checked Out'");

                #Calculate due date(3 weeks from now as per the requirements)
                transactionDate = DateTime.now
                dueDate = transactionDate + 21

                #Format datetime before adding to CSV                
		transactionDate = transactionDate.strftime("%m/%d/%Y %H:%M")
                dueDate = dueDate.strftime("%m/%d/%Y")

                #INSERT values derived from this method into Transaction table
                con.query("INSERT INTO Transactions(Inventory_ID, Action, memberID, transactionDate, dueDate) VALUES ('#{inventoryID}','#{action}','#{memberID}','#{transactionDate}','#{dueDate}')")

		#Return Due Date to UI
		returnValue = "#{dueDate}"
		return returnValue

                #In case of connection error, display message
                rescue Mysql2::Error => e
                        puts "Error code: #{e.errno}"
                        puts "Error message: #{e.error}"
                        puts "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
                ensure

                # disconnect from server
                con.close if con
	end

	#This method allows us to check in a book back into the inventory.
	#
	#*Example:*
	#	someTransaction = Transaction.new
	#	someTransaction.checkIn("1234567891011")
	#
	def checkIn(isbn)
		#Set value of action, isbn, and memberID fields for transaction data
                @action = "check-in"
                @isbn = isbn

                #Set return value for UI layer
                returnValue = ""

                #Connect to the MySQL server
                con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary")
                con.query_options.merge!(:as => :array)

                #Validate ISBN
                validateISBNQuery = con.query("SELECT * FROM Book_inventory WHERE ISBN = '#{isbn}'")

                #If sql query returns no results, then there is no such book in the DB, so exit the function
                if (validateISBNQuery.count == 0)
                        returnValue = "Invalid ISBN"
                        return returnValue
                        
                end

                #Get InventoryID of book to be checked in
                inventoryIDQuery = con.query("SELECT Inventory_ID FROM Book_inventory WHERE ISBN = '#{isbn}' AND Status = 'Checked Out' ORDER BY Inventory_ID LIMIT 1")

                #If all books matching the passed ISBN are 'Available', exit the function
                if (inventoryIDQuery.count == 0)
                        returnValue = "Already Available"
                        return returnValue
                        
                end

                inventoryID = ""
                inventoryIDQuery.each do |row|
                        inventoryID = row[0].to_int
                end

                #Inner Query - Retrieve uniqueID of the first 'Checked Out' book matching the requested ISBN
                #Outer Query - Update the Status of that retrieved book to 'Available'
                updateQuery = con.query("UPDATE Book_inventory SET Status = 'Available' WHERE Inventory_ID = '#{inventoryID}'")

                #Format datetime before adding to CSV
                transactionDate = DateTime.now
                transactionDate = transactionDate.strftime("%m/%d/%Y %H:%M")

                #INSERT values derived from this method into Transaction table
                con.query("INSERT INTO Transactions(Inventory_ID, Action, transactionDate, dueDate) VALUES ('#{inventoryID}','#{action}','#{transactionDate}','')")

                #In case of connection error, display message
                rescue Mysql2::Error => e
                        puts "Error code: #{e.errno}"
                        puts "Error message: #{e.error}"
                        puts "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
                ensure

                # disconnect from server
                con.close if con
		
		return returnValue
	end

	#This method allows us to place book On Hold. This transaction can only take place if the book is not already On Hold. The 
	#+memberID+ is passed to reserve the book to a specific patron.
	#
	#*Example:*
	#	someTransaction = Transaction.new
	#	someTransaction.reserveBook("1234567891011","100")
	#
	def reserveBook(isbn, memberID)
		#Set value of action, isbn, and memberID fields for transaction data
                @action = "reserve"
                @isbn = isbn
                @memberID = memberID

                #Set return value for UI layer
                returnValue = ""

                #Connect to the MySQL server
                con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary")
                con.query_options.merge!(:as => :array)

                #Validate ISBN
                validateISBNQuery = con.query("SELECT * FROM Book_inventory WHERE ISBN = '#{isbn}'")

                #If sql query returns no results, then there is no such book in the DB, so exit the function
                if (validateISBNQuery.count == 0)
                        returnValue = "Invalid ISBN"
                        return returnValue
                        
                end

                #Validate member
                validateMemberQuery = con.query("SELECT * FROM Member_list WHERE memberID = '#{memberID}'")

                #If sql query returns no results, then there is no such member in the DB, so exit the function
                if (validateMemberQuery.count == 0)
                        returnValue = "Invalid memberID"
                        return returnValue
                        
                end

                #Get InventoryID of book to be Reserved
                inventoryIDQuery = con.query("SELECT Inventory_ID FROM Book_inventory WHERE ISBN = '#{isbn}' AND Status <> 'On Hold' ORDER BY Inventory_ID LIMIT 1")

                #If all books matching the passed ISBN are 'On Hold', exit the function
                if (inventoryIDQuery.count == 0)
                        returnValue = "Already Reserved"
                        return returnValue
                end

                #Set the value of the unique book ID for Transaction data
                inventoryID = ""
                inventoryIDQuery.each do |row|
                        inventoryID = row[0].to_int
                end

                #Update the status of that book to On Hold
                updateQuery = con.query("UPDATE Book_inventory SET Status = 'On Hold' WHERE Inventory_ID = '#{inventoryID}'")

                #Format datetime before adding to CSV
                transactionDate = DateTime.now
                transactionDate = transactionDate.strftime("%m/%d/%Y %H:%M")

                #INSERT values derived from this method into Transaction table
                con.query("INSERT INTO Transactions(Inventory_ID, Action, memberID, transactionDate, dueDate) VALUES ('#{inventoryID}','#{action}','#{memberID}','#{transactionDate}','')")

                #In case of connection error, display message
                rescue Mysql2::Error => e
                        puts "Error code: #{e.errno}"
                        puts "Error message: #{e.error}"
                        puts "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
                ensure

                # disconnect from server
                con.close if con

                return returnValue
	end
end

#end
