#!/usr/bin/ruby

# == This class serves the following functionality:
# * <b>Add a book to the library inventory</b>
# * <b>Search for a book in the library inventory</b>
# * <b>Remove a book from the library inventory</b>

#module Smartlibrary

class Book
require 'rubygems'
require 'mysql2'
	
	#This is the ISBN number that identifies each book
	attr_reader   :isbn
	
	#This is the book's Title
	attr_reader   :title 
	
	#This is the book's Author
	attr_reader   :author

	#This is the book's location within the library
	attr_accessor :location

	#This is the current status of the book. Possible values are:
	#* Available
	#* Checked Out
	#* On Hold
	attr_accessor :status

	#Constructor - Sets values of all class attributes to blank, except +status+, which is set to _Available_ by default
	def initialize
		@isbn = ""
		@title = ""
		@author = ""
		@location = ""
		@status = "Available"
	end
	
	#This method is responsible for adding a book to the library inventory
	#
	#We pass 4 arguments into this method: +isbn+, +title+, +author+, and +location+,
	#which are set to the instance variables and used to open and append a row to the CSV
	#file containing the library's inventory of books
	#
	#*Example:*
	#     mybook = Book.new
	#     mybook.addToInventory("1234567891011","Sample Book","John Doe","E900")
	#
	def addToInventory(isbn, title, author, location)		
	
		#Set value of instance variables to match the arguments
                @isbn = isbn
                @title = title
                @author = author
                @location = location

                #Connect to the MySQL server
		con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary") 

                con.query("INSERT INTO Book_inventory(ISBN, Title, Author, Location, Status) VALUES ('#{isbn}', '#{title}', '#{author}', '#{location}', '#{status}')")
		confirmation = "'#{title}' by #{author}"		

                #In case of connection error, display message
               	rescue Mysql2::Error => e
                        confirmation =  "Error code: #{e.errno}, Error message: #{e.error}"
                ensure
                
		#Disconnect from server
                con.close if con

                return confirmation
		
	end

	#This method is responsible for searching for a book in the library's inventory
	#
	#Only +isbn+ is needed as an argument. This value is used to search the first column of the
	#CSV file (ISBN). 
	#
	#If a partial ISBN passed, all isbn's matching the partial search criteria will be retrieved from the CSV
	#
	#<b>Example 1:</b>
	#	mybook = Book.new
	#	mybook.searchInventory("1234567891011")
	#
	#<b>Example 2:</b>
	#	mybook = Book.new.searchInventory("123")
	#
	def searchInventory(isbn)
		@isbn = isbn		
		bookArray = Array.new

		#Connect to the MySQL server
                con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary")
                #This allows the result set to be manipulated into an array
		con.query_options.merge!(:as => :array)

                results = con.query("SELECT ISBN, Title, Author, Location, Status FROM Book_inventory WHERE ISBN LIKE '#{isbn}%'")
		# Push each result into a new element in an array                                                                                                         
                results.each do |row|
                        bookArray.push(row)
                end

                #In case of connection error, display message
                rescue Mysql2::Error => e
                        bookArray.push("Error code: #{e.errno}")
                        bookArray.push("Error message: #{e.error}")
                        bookArray.push("Error SQLSTATE: #{e.sqlstate}") if e.respond_to?("sqlstate")
                ensure

                # disconnect from server
                con.close if con

		return bookArray

	end


	#This method is responsible for removing a book from the library's inventory.
	#
	#Only +isbn+ is needed as an argument, but the *complete* isbn is required.
	#
	#If the isbn is not found in any of the rows of the first column of the CSV file, the method will exit.
	#
	#*Example:*
	#	mybook = Book.new
	#	mybook.removeBook("1234567891011")
	#
	def removeBook(isbn)
                @isbn = isbn
                success = true

                #Connect to the MySQL server
                con = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "GetMysql45", :database => "SmartLibrary")

                validateISBNQuery = con.query("SELECT * FROM Book_inventory WHERE ISBN = '#{isbn}'")

                #If sql query returns no results, then there is no such book in the DB, so exit the function
                if (validateISBNQuery.count == 0)
                        success = false
                        return success                                                                                                                                    
                end

                deletebookQuery = con.query("DELETE FROM Book_inventory WHERE ISBN = '#{isbn}'")                                                                          

                #In case of connection error, display message
                rescue Mysql2::Error => e
                        puts "Error code: #{e.errno}"
                        puts "Error message: #{e.error}"
                        puts "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
                        success = false                                                                                                                                   
                ensure
                # disconnect from server
                con.close if con

                return success
	
	end

end

#end
