#!/usr/bin/ruby

require 'cgi'
require_relative '../lib/Patron'

query = CGI.new("html5")

query.out do
        query.html do
                query.head { query.title { "SmartLibrary: Search Patron" } } +
                query.body do
                        query.form("ACTION" => "prog/searchPatronUI.rb") do
                                query.h1 { "SmartLibrary" } +
                                query.h2 { "Find a Library Member" } +
                                query.hr +
                                "<p>Enter Patron Last Name:   " + query.text_field("lastName") +
                                query.submit("VALUE" => "Search") +
                                "</p>"
                        end
                end
        end
end

